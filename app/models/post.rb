require 'active_model'

class Post
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :title, :body, :blog, :pubdate, :image_url

  validates :title, :presence => true

  def initialize(attrs ={})
    attrs.each { |k,v| send("#{k}=", v) }

  end

  def publish(clock = DateTime)
    return false unless valid?
    blog.pubdate = clock.now
    blog.add_entry(self)
    return true
  end

  def persisted?
    false
  end

  def picture?
    image_url.present?
  end

  def self.first_before(date)
    first(conditions: ["pubdate < ?", date],
          order:      "pubdate DESC")
  end

  def self.first_after(date)
    first(conditions: ["pubdate > ?", date],
          order:      "pubdate ASC")
  end

  def prev
    self.class.first_before(pubdate)
  end

  def next
    self.class.first_after(pubdate)
  end

  def up
    THE_BLOG
  end
end
