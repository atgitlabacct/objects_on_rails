require 'delegate'

class Exhibit < SimpleDelegator
  def initialize(model, context)
    @context = context
    super(model)
  end

  def render(partial)
    @context.render(partial: partial, locals: {post: self})
  end

  def to_model
    __getobj__
  end

  def class
    __getobj__.class
  end
end
