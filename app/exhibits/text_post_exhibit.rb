require_relative 'exhibit'

class TextPostExhibit < Exhibit 
  def render_body
    render "/posts/text_body"
  end
end
