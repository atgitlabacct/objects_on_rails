require_relative 'exhibit'

class PicturePostExhibit < Exhibit 

  def render_body
    render "/posts/picture_body"
  end
end
