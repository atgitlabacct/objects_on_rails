require_relative '../spec_helper_lite'
require_relative '../../app/models/post'
require 'ostruct'

describe Post do
  let(:blog) { mock('Blog').as_null_object }

  it "should start with blank attributes" do
    subject.title.should be_nil
    subject.body.should be_nil
    subject.blog.should be_nil
  end

  it "should support reading and writing a title" do
    subject.title= "foo"
    subject.title.should eql("foo")
  end

  it "should support reading and writing a post body" do
    subject.body = "foo"
    subject.body.should eql "foo"
  end

  it "should support setting attributes in the intializer" do
    subject = Post.new(:title => 'mytitle', :body => 'mybody')
    subject.title.should eql('mytitle')
    subject.body.should eql 'mybody'
  end

  it "should support reading and writing a blog reference" do
    blog = Object.new
    subject.blog = blog
    subject.blog.should eql blog
  end

  it "should not be valid without a title" do
    ['', ' ', nil].each do |bad_title|
      subject.title = bad_title
      subject.valid?.should be_false
    end
  end

  it "should be valid with a non blank title" do
    subject.title = "foo"
    subject.valid?.should be_true
  end


  describe '#publish' do
    before(:each) do
      subject.blog = blog
      subject.title = "valid"
    end

    it "should add the post to the blog" do
      blog.should_receive(:add_entry).with(subject)
      subject.publish
    end

    context "given an invalid post" do
      let (:invalid_post) { Post.new(:title => '') }

      it "won't add the post to the blog" do
        blog.should_not_receive(:add_entry)
        invalid_post.publish
      end

      it "returns false" do
        invalid_post.publish.should be_false
      end
    end
  end

  describe "#pubdate" do
    before(:each) do
      subject.blog = blog
    end
  
    context "before publishing" do
      it "should be blank" do
        subject.pubdate.should be_nil
      end
    end

    context "given an invalid post" do

      it "should not add the post to the blog" do
        blog.should_not_receive(:add_entry)
        subject.publish
      end

      it "should return false" do
        subject.publish.should eql(false)
      end
    end

    context "given a valid post" do
      before(:each) do
        subject.title = "valid"
      end

      context "when publishing" do
        it "should add the post to the blog" do
          blog.should_receive(:add_entry).with(subject)
          subject.publish
        end

        it "should update the pubdate" do
          time = DateTime.now
          clock = stub('Time', :now => time)

          blog.should_receive(:pubdate=).with(time)
          subject.blog = blog
          subject.publish(clock)
        end
      end
    end
  end

  describe "#picture?" do
    it "should be true when the post has a picture url" do
      subject.image_url = "http://something/something.gif"
      subject.picture?.should be_true
    end

    it "should be false when the post does not have a picture" do
      subject.image_url = ""
      subject.picture?.should be_false
      subject.image_url = nil
      subject.picture?.should be_false
    end
  end
end
