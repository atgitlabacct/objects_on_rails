require 'ostruct'
require_relative '../../app/models/blog'

describe Blog do
  it "has no entries" do
    subject.should have(0).entries
  end

  describe "#new_post" do
    let(:new_post) { OpenStruct.new }

    before(:each) do
      subject.post_maker = -> { new_post }
    end

    it "should return a new post" do
      subject.new_post.should eql new_post
    end

    it "should set the post's blog reference to itself" do
      subject.new_post.blog.should eql(subject)
    end

    it "should accept an attribute hash on behalf of the post maker" do
      post_maker = mock('Post Maker')
      post_maker.should_receive(:call).with(:title => 'foo', :body => 'bar').and_return new_post
      subject.post_maker = post_maker
      subject.new_post(:title => 'foo', :body => 'bar')
    end
  end

  describe "#add_entry" do
    it "should add the entry to the blog" do
      entry = double
      entry.stub(:pubdate)

      subject.add_entry(entry)
      subject.entries.should include(entry)
    end
  end

  describe "#entries" do
    def stub_entry_with_date(date)
      entry = double("Entry")
      entry.stub(:pubdate) { DateTime.parse(date) }
      entry
    end

    it "should be sorted in reverse-chronological order" do
      oldest = stub_entry_with_date('2011-09-09')
      newest = stub_entry_with_date('2011-09-11')
      middle = stub_entry_with_date('2011-09-10')

      subject.add_entry(oldest)
      subject.add_entry(newest)
      subject.add_entry(middle)

      subject.entries.should eql([newest, middle, oldest])
    end

    it "is limited to 10 items" do
      10.times do |i|
        subject.add_entry(stub_entry_with_date("2012-01-#{i+1}"))
      end

      oldest = stub_entry_with_date('2010-01-01')
      subject.add_entry(oldest)

      entries = subject.entries
      subject.entries.should_not include (oldest)
      subject.entries.size.should eql(10)
    end
  end
end
