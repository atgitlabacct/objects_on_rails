require_relative '../spec_helper_lite'
require_relative '../../app/exhibits/text_post_exhibit'
require 'ostruct'

describe TextPostExhibit do
  let(:post) { OpenStruct.new(title: "TITLE", body: "BODY", pubdate: "PUBDATE") }
  let(:context) { mock('Template') }
  subject { TextPostExhibit.new(post, context) }

  it "delegates method calls to post" do
    subject.title.should eql("TITLE")
    subject.body.should eql("BODY")
    subject.pubdate.should eql("PUBDATE")
  end

  it "renders itself with appropriate partial" do
    context.should_receive(:render).with(partial: "/posts/text_body", locals: {post: subject}).and_return "THE HTML"
    subject.render_body.should eql("THE HTML")
  end
end
