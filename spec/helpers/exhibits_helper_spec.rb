require_relative '../spec_helper_lite'
require_relative '../../app/helpers/exhibits_helper'

stub_class 'PicturePostExhibit'
stub_class 'TextPostExhibit'
stub_class 'Post'

describe ExhibitsHelper do
  subject { Object.new} 
  let(:post) { Post.new }
  let(:context) { double('Context') }

  before(:each) do
    subject.extend(ExhibitsHelper)
  end

  it "decorates picture posts with a PicturePostExhibit" do
    post.stub(:picture?) { true }
    subject.exhibit(post, context).should be_kind_of(PicturePostExhibit)
  end

  it "decorates text posts with a TextPostExhibit" do
    post.stub(:picture?) { false }
    subject.exhibit(post, context).should be_kind_of(TextPostExhibit)
  end

  it "leaves objects it doesn't know about alone" do
    post.stub(:picture?) { false }
    subject.extend(ExhibitsHelper)
    dummy = double
    subject.exhibit(dummy, context).should eql(dummy)
  end

end
